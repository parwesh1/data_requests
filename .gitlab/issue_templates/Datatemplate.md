# Checklist (make sure you check all the steps)

- [ ] Give your issue a title (pillar name - short description)
- [ ] Fill in the template question
- [ ] Select your pillar with Epic (if your request is not in a pillar select Other)
- [ ] Set the priority with labels (~"prio:1", 2 and 3)
- [ ] Select create issue

# Data-request questions (fill these in as complete as you can)

## What data insight do you want to get?
Here you should describe in short detail what kind of data the insight contains. 

Example answer:\
_Ik wil per business klant kunnen zien hoevaak in een bepaald termijn (week/maand/jaar) een bepaalde koerier daar langs komt._

## Why do you want to get this data insight?
Here you should explain the reason why you want this insight, maybe to make a certain decision?

Example answer:\
Because then we can get a better overview of whether couriers return more often to a certain business customer. This allows us to better target couriers (this is a fabricated reason).

## What filters do you want to add to the insight?
Here you should write wich filters you want in the insight.
some exampels are filtering on business account names, date and courier name.

Example answer:\
- filter by date (a date range)
- filter on a business account name (like to be able to select several in 1x)
- filter on a courier name


## Do you want to group the data
Here you should make clear is the data in the insight must be grouped. this means that the data can grouped by a certain element such as date and names

Example answer:\
_on date: per week, per month, per year_


# Example output (not mandatory)
Here you should put a example of the output if you already know what is is. this helps us to make the data request more accurate to your needs. (Use the table below it is now filled with example data)

| Business customers  | Koerier 1      | Koerier 2      | Koerier 3     | Koerier 4     |
|-------              |--------------- |--------------- |---------------|---------------|
| Business customer 1 | 20             | 60             | 10            | 5             |
| Business customer 2 | 40             | 70             | 18            | 3             |
| Business customer 3 | 50             | 80             | 20            | 2             |
